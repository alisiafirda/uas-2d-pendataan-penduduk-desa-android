package safira.alisia.aplikasiuas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat.startActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity  : AppCompatActivity(), View.OnClickListener ,
    BottomNavigationView.OnNavigationItemSelectedListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnData.setOnClickListener(this)
        bottomNavigationView.setOnNavigationItemSelectedListener (this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnData -> {
                var intent = Intent(this, PendudukActivity::class.java)
                startActivity(intent)
            }
        }
    }


    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.itemInfo -> {
                val i = Intent(this, InfoActivity::class.java)
                startActivity(i)

            }
            R.id.itemData -> {
                val intent = Intent(this, PendudukActivity::class.java)
                startActivity(intent)

            }
            R.id.itemScan -> {
            val s = Intent(this, BarcodeActivity::class.java)
            startActivity(s)

            }
            R.id.itemVidio -> {
                val s = Intent(this, VidioActivity::class.java)
                startActivity(s)

            }
            R.id.itemBeranda -> FrameLayout.visibility = View.GONE
        }
        return true
    }
}


