package safira.alisia.aplikasiuas

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_data.*

class AdapterDataPenduduk(
    val dataPenduduk: List<HashMap<String,String>>,
    val mainActivity: PendudukActivity
) :
    RecyclerView.Adapter<AdapterDataPenduduk.HolderDataPenduduk>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterDataPenduduk.HolderDataPenduduk {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_penduduk,parent,false)
        return HolderDataPenduduk(v)
    }

    override fun getItemCount(): Int {
        return dataPenduduk.size
    }

    override fun onBindViewHolder(holder: AdapterDataPenduduk.HolderDataPenduduk, position: Int) {
        val data = dataPenduduk.get(position)
        holder.txNik.setText(data.get("nik"))
        holder.txNmPenduduk.setText(data.get("nama"))
        holder.txJenkel.setText(data.get("jenkel"))
        holder.txDusun.setText(data.get("nm_dusun"))
        holder.txPekerjaan.setText(data.get("pekerjaan"))
        holder.txPekerjaan.setText(data.get("tipe_ekonomi"))

        if (position.rem(2) == 0) holder.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else holder.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        holder.cLayout.setOnClickListener(View.OnClickListener {
            val pos = mainActivity.daftarJenkel.indexOf(data.get("jenkel"))
            mainActivity.spinJenkel.setSelection(pos)
            mainActivity.edNik.setText(data.get("nik"))
            mainActivity.edNamaPenduduk.setText(data.get("nama"))
            Picasso.get().load(data.get("url")).into(mainActivity.imUpload);
            mainActivity.edDusun.setText(data.get("nm_dusun"))
            mainActivity.edPekerjaan.setText(data.get("pekerjaan"))
            mainActivity.edEkonomi.setText(data.get("tipe_ekonomi"))
        })

        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.photo);
    }

    class HolderDataPenduduk(v : View) : RecyclerView.ViewHolder(v){
        val txNik = v.findViewById<TextView>(R.id.txNik)
        val txNmPenduduk = v.findViewById<TextView>(R.id.txNmPenduduk)
        val txJenkel = v.findViewById<TextView>(R.id.txJenkel)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val txDusun = v.findViewById<TextView>(R.id.txDusun)
        val txPekerjaan = v.findViewById<TextView>(R.id.txPekerjaan)
        val txEkonomi = v.findViewById<TextView>(R.id.txEkonomi)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }

}