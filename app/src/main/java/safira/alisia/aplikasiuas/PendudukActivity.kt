package safira.alisia.aplikasiuas

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_data.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class PendudukActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUpload ->{
                requestPermissions()
            }
            R.id.btnInsert ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate ->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete ->{
                queryInsertUpdateDelete("delete")
            }
        }
    }

    lateinit var mediaHelper: MediaHelper
    lateinit var pendudukAdapter : AdapterDataPenduduk
    lateinit var jenkelAdapter : ArrayAdapter<String>
    var daftarPenduduk = mutableListOf<HashMap<String,String>>()
    var daftarJenkel = mutableListOf<String>()
    var url = "http://192.168.43.60/uas-2d-pendataan-penduduk-desa-web/show_data.php"
   var url2 = "http://192.168.43.60/uas-2d-pendataan-penduduk-desa-web/get_nama_dusun.php"
    var url3 = "http://192.168.43.60uas-2d-pendataan-penduduk-desa-web/query_ins_upd_del.php"
//    var url = "http://192.168.43.34/pendataan/show_data.php"
//    var url2 = "http://192.168.43.34/pendataan/get_nama_dusun.php"
//    var url3 = "http://192.168.43.34/pendataan/query_ins_upd_del.php"
    var imStr = ""
    var pilihJenisKelamin = ""
    var nmFile = ""
    var fileUri = Uri.parse("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data)
        pendudukAdapter = AdapterDataPenduduk(daftarPenduduk,this)
        mediaHelper = MediaHelper(this)
        listPenduduk.layoutManager = LinearLayoutManager(this)
        listPenduduk.adapter = pendudukAdapter

        jenkelAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarJenkel)
        spinJenkel.adapter = jenkelAdapter
        spinJenkel.onItemSelectedListener = itemSelected

        imUpload.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataPenduduk("")
        getNamaProdi()
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinJenkel.setSelection(0)
            pilihJenisKelamin = daftarJenkel.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihJenisKelamin = daftarJenkel.get(position)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode==mediaHelper.getRcCamera()){
                imStr = mediaHelper.getBitmapToString(imUpload,fileUri)
                nmFile = mediaHelper.getMyFileName()
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("nik")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil",Toast.LENGTH_SHORT).show()
                    showDataPenduduk("")
                }else{
                    Toast.makeText(this,"Operasi GAGAL",Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server",Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("nik",edNik.text.toString())
                        hm.put("nama",edNamaPenduduk.text.toString())
                        hm.put("jenkel",pilihJenisKelamin)
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nm_dusun",edDusun.text.toString())
                        hm.put("pekerjaan",edPekerjaan.text.toString())
                        hm.put("tipe_ekonomi",edEkonomi.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("nik",edNik.text.toString())
                        hm.put("nama",edNamaPenduduk.text.toString())
                        hm.put("jenkel",pilihJenisKelamin)
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nm_dusun",edDusun.text.toString())
                        hm.put("pekerjaan",edPekerjaan.text.toString())
                        hm.put("tipe_ekonomi",edEkonomi.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("nik",edNik.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun requestPermissions() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }

    fun getNamaProdi(){
        val request = StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarJenkel.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarJenkel.add(jsonObject.getString("jenkel"))
                }
                jenkelAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataPenduduk(namaPenduduk : String){
        val request = object : StringRequest(Request.Method.POST,url,
            Response.Listener { response ->
                daftarPenduduk.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var penduduk = HashMap<String,String>()
                    penduduk.put("nik",jsonObject.getString("nik"))
                    penduduk.put("nama",jsonObject.getString("nama"))
                    penduduk.put("jenkel",jsonObject.getString("jenkel"))
                    penduduk.put("url",jsonObject.getString("url"))
                    penduduk.put("nm_dusun",jsonObject.getString("nm_dusun"))
                    penduduk.put("pekerjaan",jsonObject.getString("pekerjaan"))
                    penduduk.put("tipe_ekonomi",jsonObject.getString("ekonomi"))
                    daftarPenduduk.add(penduduk)
                }
                pendudukAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama",namaPenduduk)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

}
