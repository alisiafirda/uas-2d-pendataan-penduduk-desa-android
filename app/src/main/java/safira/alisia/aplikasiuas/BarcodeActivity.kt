package safira.alisia.aplikasiuas

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import androidx.appcompat.app.AppCompatActivity
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_barcode.*

class BarcodeActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View){
        when(v?.id){
            R.id.btnScanQR -> {
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btnGenerateQR -> {
                val barCodeEncoder = BarcodeEncoder()
                val bitmap = barCodeEncoder.encodeBitmap(edQrCode.text.toString(),
                    BarcodeFormat.QR_CODE, 400, 400)
                imV.setImageBitmap(bitmap)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val intentResult = IntentIntegrator.parseActivityResult(requestCode,requestCode,data)
        if(intentResult!=null){
            if(intentResult.contents != null){
                edQrCode.setText(intentResult.contents)
            }else{
                Toast.makeText(this, "Dibatalkan", Toast.LENGTH_SHORT).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    lateinit var intentIntegrator: IntentIntegrator

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_barcode)

        intentIntegrator = IntentIntegrator(this)
        btnGenerateQR.setOnClickListener(this)
        btnScanQR.setOnClickListener(this)
    }
}